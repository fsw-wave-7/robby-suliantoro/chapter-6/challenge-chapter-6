const { Usergh } = require('../../models')
const { Usergb } = require('../../models') 
const { Userg } = require('../../models')
const { join } = require('path')
const bcrypt = require("bcrypt")

class HomeController {

    index = (req, res) => {
        Userg.findAll()
        .then(users => {
            res.render(join(__dirname, '../../views/index'), {
                content: './pages/userList',
                users: users
          })
        })
    }

    add = (req, res) => {
        res.render(join(__dirname, '../../views/index'), {
            content: './pages/addList'
        })
    }

    saveUser = async (req, res) => {

        const salt = await bcrypt.genSalt(10);

        Userg.create({
            userid: req.body.userid,
            username: req.body.username,
            password: await bcrypt.hash(req.body.password, salt),
            Usergb: {
                name: req.body.name,
                address: req.body.address,
                handphone: req.body.handphone,
            },
            Usergh: {
                time: req.body.time,
                score: req.body.score,
            },
        },
        {
            include : [
                {
                    model: Usergb,
                    as: "Usergb",
                },
                {
                    model: Usergh,
                    as: "Usergh"
                },

            ],
            
        }
        )
        .then(() => {
            res.redirect('/');
        }) 
        .catch(err => {
            console.log(err);
        })

    }

    edit = (req, res) => {

        const index = req.params.id;
        //SEQUELIZE CODE
        Userg.findOne({
            where: {id: index},

        }).then((userg) => {
        res.render(join(__dirname, '../../views/index'), {
            content: './pages/editList',
            userg: userg,
        })

    })
    }

    update = async (req, res) => {

        const salt = await bcrypt.genSalt(10)

        Userg.update({
            userid: req.body.userid,
            username: req.body.username,
            password:  await bcrypt.hash(req.body.password, salt)
        }, {
        where: { id: req.params.id }
        },
        {
            include: {
                model: Usergb,
                as: "Usergb"
            }
        }
    
        )

        .then(() => {
            res.redirect('/')
        }) .catch(err => {
            res.status(422).json("Can't update user")
        })
    }
       

    delete = (req, res) => {
        Userg.destroy({
            where: {
                id: req.params.id
            }
        })
        .then(() => {
            res.redirect('/')
        })
    }

}

module.exports= HomeController