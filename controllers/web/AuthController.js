const { join } = require('path')
const { Userg } = require('../../models')
const bcrypt = require("bcrypt")

class AuthController {

    login = (req, res) => {
        res.render(join(__dirname, '../../views/login'))
    }
    
    doLogin = async (req, res) => {
        const body = req.body;
        
        if(!(body.username && body.password)) {
            return res.status(400).send({ error: "Data not formatted properly"});
        }

        Userg.findOne({
            where: {username: body.username }
        })
        .then(userg => {
            bcrypt.compare(body.password, userg.password, (err, data) => {
                if (err) throw err

                if (data) {
                        res.cookie('loginData', JSON.stringify(userg))
                        res.redirect('/')
                } else {
                    return res.status(401).json({ msg: "Password Salah"})
                }
            
            });
        })

        .catch(err => {
            return res.status(401).json({ msg: "User Tidak Ditemukan"})
        })

    }

    logout = (req, res) => {
        res.clearCookie('loginData')
        res.redirect('/')
    }

}

module.exports = AuthController