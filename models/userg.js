'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Userg extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      Userg.hasOne(models.Usergb, {
        as: "Usergb",
        foreignKey :"userid",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
      Userg.hasMany(models.Usergh, {
        as: "Usergh",
        foreignKey: "userid",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      })
    }
  };
  Userg.init({
    userid: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Userg',
  });
  return Userg;
};