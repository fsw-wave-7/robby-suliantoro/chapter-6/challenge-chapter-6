'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Usergb extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Usergb.belongsTo(models.Userg, {
        as: "Usergb",
        foreignKey: "userid",
        onDelete: "CASCADE",
      });
    }
  };
  Usergb.init({
    userid: DataTypes.STRING,
    name: DataTypes.STRING,
    address: DataTypes.STRING,
    handphone: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Usergb',
  });

  // Usergb.associate = function(models) {
  //   Usergb.belongsTo(models.Userg, {foreignKey: 'userid', as: 'Userg'})
  // };

  return Usergb;
};