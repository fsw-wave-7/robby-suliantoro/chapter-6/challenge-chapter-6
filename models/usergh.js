'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Usergh extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      Usergh.associate = (models) => {
        Usergh.belongsTo(models.Userg, {
          as: "Usergh",
          foreignKey: "userid",
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
      });
    };
    }
  };
  Usergh.init({
    userid: DataTypes.STRING,
    time: DataTypes.INTEGER,
    score: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Usergh',
  });
  return Usergh;
};