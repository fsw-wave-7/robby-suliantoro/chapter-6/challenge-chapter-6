const { Router } = require('express')
const { join } = require('path')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')

const AuthController = require('../controllers/web/AuthController')
const HomeController = require('../controllers/web/HomeController')
const AuthMiddleware = require('../middlewares/AuthMiddleware');

const web = Router()

web.use(bodyParser.json())
web.use(bodyParser.urlencoded({ extended: true}))
web.use(cookieParser())

const authController = new AuthController
const homeController = new HomeController

web.get('/login', authController.login)
web.post('/login', authController.doLogin)
web.get('/logout', authController.logout)


web.use(AuthMiddleware())

web.get('/', homeController.index)
web.post('/save-user', homeController.saveUser)
web.get('/addList', homeController.add)


web.get('/editList/:id', homeController.edit)
web.post('/editList/:id', homeController.update)

web.get('/delete/:id', homeController.delete)

// web.get('/skor/:id', homeController.skor)
// web.post('/skor/:id', homeController.saveSkor)


// web.get('/login', (req, res) => {
//     res.render(join(__dirname, '../views/login'))
// })

// web.post('/login', (req, res) => {
//     // is login success?
//     res.render(join(__dirname, '../views/login'))
// })

// web.get('/', (req, res) => {
//     res.render(join(__dirname, '../views/index'), {
//         content: './pages/userList'
//     })
// })

// web.get('/add', (req, res) => {
//     res.render(join(__dirname, '../views/index'), {
//         content: './pages/addList'
//     })
// })

module.exports = web
